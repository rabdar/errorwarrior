import java.util.Scanner;

public class RunGame {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.println("Hello Stranger in the hell's gates of the ERROR’s world!\n" +
                "Are you ready to enter the world of ERROR?\n" +
                "Enter YES [Y] otherwise the gate will remain closed.");

        String startGame = input.nextLine().toUpperCase().trim();

        while (!(startGame.equals("YES") || startGame.equals("Y"))) {
            System.out.println("HAHAHA you are coward.\n" +
                    "Maybe it’s better to be a live coward than a dead hero.\n" +
                    "Please try again, maybe this time you’ll have more courage.");
            startGame = input.nextLine().trim().toUpperCase();
        }

        System.out.println(
                "....... ___________   ___________ .......\n" +
                        ".......|          /  |           |.......\n" +
                        ".......|         /   |           |.......\n" +
                        ".......|        /    |           |.......\n" +
                        ".......|       /     |           |.......\n" +
                        ".......|______/      |___________|.......\n" +
                        "The gate is open!\n" +
                        "It was a real ERROR ;). Your soul will never return to the world of the living HAHAHA!!!\n" +
                        "Please choose one of the game options by entering the letter from the brackets [].");

        new GameOptions().gameOptions();

        input.close();
    }
}
