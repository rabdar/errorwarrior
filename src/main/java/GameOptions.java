
import java.util.Scanner;

class GameOptions {

    void gameOptions() {

        System.out.println(
                "\n*NEW GAME* [N]\n" +
                        "***LOAD*** [L]\n" +
                        "***EXIT*** [E]\n"
        );

        while (true) {
            Scanner input = new Scanner(System.in);
            String option = input.nextLine().trim().toUpperCase();

            switch (option) {
                case "N":
                    new NewGame().newGame();
                    break;
                case "L":
                    User user = new FileReaderGame().load();
                    System.out.println("game loaded correctly");
                    user.getUserInterface().userInterface(user);
                    break;
                case "E":
                    System.out.println("Game End");
                    break;
                default:
                    System.out.println("The option you’re trying to choose doest exist. " +
                            "Please enter a letter from [].\n");
                    continue;
            }
            break;
        }
    }
}


