import createHero.Hero;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class FileWriterGame {
    public void save(User user) {
        String fileName = "testFile.txt";
        try (
                var fileWriter = new FileWriter(fileName);
                var writer = new BufferedWriter(fileWriter)
        ) {
            writer.write(user.getName());
            writer.newLine();
            for (Hero heroes : user.getHeroList()) {
                writer.write(String.valueOf(heroes));
                writer.newLine();
            }
        } catch (IOException e) {
            System.err.println("Nie udało się zapisać pliku " + fileName);
        }
    }
}
