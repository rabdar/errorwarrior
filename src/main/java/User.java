import createHero.*;

import java.util.ArrayList;

public class User {

    private String name;
    private ArrayList<Hero> heroList = new ArrayList<>();
    private final UserInterface userInterface = new UserInterface();

    ArrayList<Hero> getHeroList() {
        return heroList;
    }

    public void setHeroList(ArrayList<Hero> heroList) {
        this.heroList = heroList;
    }

    User() {
    }

    User(Hero hero1, Hero hero2, String name) {
        this.name = name;
        this.heroList.add(hero1);
        this.heroList.add(hero2);
    }


    String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserInterface getUserInterface() {
        return userInterface;
    }

    @Override
    public String toString() {
        StringBuilder nameHeroes = new StringBuilder();
        for (Hero heroes : heroList) {
            nameHeroes.insert(0, "\n" + heroes.toString());
        }
        return "Your name: " + name + "\nYour heroes: " + nameHeroes;
    }
}
