import createHero.heroClass.Archer;
import createHero.heroClass.Fairy;
import createHero.heroClass.Magician;
import createHero.heroClass.Warrior;

import java.util.Scanner;

public class NewGame {

    void newGame() {
        Scanner input = new Scanner(System.in);

        System.out.println("As the world was embraced by the impenetrable darkness of ERROR, YOU appeared!\n" +
                "Defeat all enemies and the ERROR will be solved.\n" +
                "What is your name?\n");
        String name = input.nextLine().trim();

        if (!name.equals("")) {
            System.out.println(name + " welcome to the world dominated by ERROR. ");
        } else {
            name = "LOSER";
            System.out.println("If you don’t want to enter your name, I’ll call you a LOSER.\n" +
                    name + " welcome in the world of ERROR HAHAHA");
        }


        System.out.println("You’re represented by heroes who’re ready to fight on your behalf.\n" +
                "Choose your own team who will help you freed the world from the ERROR.\n" +
                "Please choose one pair by entering the letter from the brackets [].\n" +
                "WARRIOR and FAIRY [WF]\n" +
                "or\n" +
                "ARCHER and MAGICIN [AM]");


        String choicesHeroes = input.nextLine().trim().toUpperCase();

        while (!(choicesHeroes.equals("WF") || choicesHeroes.equals("AM"))) {
            System.out.println("The option you’re trying to choose doest exist. \n" +
                    "Please enter a letter from [].");
            choicesHeroes = input.nextLine().trim().toUpperCase();
        }

        User user = new User();

        switch (choicesHeroes) {
            case "WF":
                user = new User(new Warrior(1), new Fairy(1), name);
                break;
            case "AM":
                user = new User(new Archer(1), new Magician(1), name);
                break;
        }

        System.out.println("\nCongratulations! " + user.getName().toUpperCase() +
                "\nYou have successfully created your character. You can start the fight with ERROR by selecting FIGHT [F]" +
                "\nYou can check your user profile in the HEROES [H] panel.");
        user.getUserInterface().userInterface(user);
    }

}
