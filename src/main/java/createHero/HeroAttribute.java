package createHero;

public class HeroAttribute {
    private int lvl;
    private int strength;
    private int energy;
    private int live;
    private int armor;
    private int dexterity;

    int getLvl() {
        return lvl;
    }

    public HeroAttribute setLvl(int lvl) {
        this.lvl = lvl;
        return this;
    }

    int getStrength() {
        return strength;
    }

    public HeroAttribute setStrength(int strength) {
        this.strength = strength;
        return this;
    }

    int getEnergy() {
        return energy;
    }

    public HeroAttribute setEnergy(int energy) {
        this.energy = energy;
        return this;
    }

    int getLive() {
        return live;
    }

    public HeroAttribute setLive(int live) {
        this.live = live;
        return this;
    }

    int getArmor() {
        return armor;
    }

    public HeroAttribute setArmor(int armor) {
        this.armor = armor;
        return this;
    }

    int getDexterity() {
        return dexterity;
    }

    public HeroAttribute setDexterity(int dexterity) {
        this.dexterity = dexterity;
        return this;
    }

    @Override
    public String toString() {
        return ": lvl=" + lvl +
                ", strength=" + strength +
                ", energy=" + energy +
                ", live=" + live +
                ", armor=" + armor +
                ", dexterity=" + dexterity;
    }
}
