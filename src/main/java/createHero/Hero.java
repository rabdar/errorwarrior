package createHero;

public abstract class Hero {

    protected HeroAttribute heroAttribute;

    public Hero() {
        heroAttribute = new HeroAttribute();
    }

    public int standardAttack() {
        return heroAttribute.getStrength();
    }

    public int energyAttack() {
        return heroAttribute.getStrength();
    }

    public int hitDefence() {
        return (heroAttribute.getDexterity() * heroAttribute.getArmor()) / heroAttribute.getLvl();
    }

    public void lvlUp() {
        heroAttribute.setLvl(heroAttribute.getLvl() + 1);
        heroAttribute.setLive(heroAttribute.getLive() * heroAttribute.getLvl());
        heroAttribute.setStrength(heroAttribute.getStrength() * heroAttribute.getLvl());
        heroAttribute.setEnergy(heroAttribute.getEnergy() * heroAttribute.getLvl());
        heroAttribute.setArmor(heroAttribute.getArmor() * heroAttribute.getLvl());
        heroAttribute.setDexterity(heroAttribute.getDexterity() * heroAttribute.getLvl());
    }

    public int getHeroLvl() {
        return heroAttribute.getLvl();
    }

    public double getHeroEnergy() {
        return heroAttribute.getEnergy();
    }

    public int getHeroLive() {
        return heroAttribute.getLive();
    }

    public abstract void setHeroAttribute(int lvl);

    @Override
    public String toString() {
        return getClass().getSimpleName() + heroAttribute.toString();
    }

}
