package createHero.heroClass;

import createHero.Hero;

public class Fairy extends Hero {

    public Fairy (int lvl){
        super();
        setHeroAttribute(lvl);
    }

    @Override
    public void setHeroAttribute(int lvl){
        heroAttribute.setLvl(lvl);
        heroAttribute.setStrength(20*lvl);
        heroAttribute.setEnergy(160*lvl);
        heroAttribute.setLive(100*lvl);
        heroAttribute.setArmor(25*lvl);
        heroAttribute.setDexterity(40*lvl);
    }

    @Override
    public int standardAttack() {
        return (5 * super.standardAttack());
    }

    @Override
    public int energyAttack() {
        return (10 * super.energyAttack());
    }
}
