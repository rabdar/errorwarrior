package createHero.heroClass;

import createHero.Hero;

public class Magician extends Hero {

    public Magician(int lvl) {
        super();
        setHeroAttribute(lvl);
    }

    @Override
    public void setHeroAttribute(int lvl) {
        heroAttribute.setLvl(lvl);
        heroAttribute.setStrength(25 * lvl);
        heroAttribute.setEnergy(80 * lvl);
        heroAttribute.setLive(100 * lvl);
        heroAttribute.setArmor(50 * lvl);
        heroAttribute.setDexterity(20 * lvl);
    }

    @Override
    public int standardAttack() {
        return (4 * super.standardAttack());
    }

    @Override
    public int energyAttack() {
        return (8 * super.energyAttack());
    }

}
