package createHero.heroClass;

import createHero.Hero;

public class Archer extends Hero {


    public Archer(int lvl) {
        super();
        setHeroAttribute(lvl);
    }

    @Override
    public void setHeroAttribute(int lvl) {
        heroAttribute.setLvl(lvl);
        heroAttribute.setStrength(50 * lvl);
        heroAttribute.setEnergy(40 * lvl);
        heroAttribute.setLive(100 * lvl);
        heroAttribute.setArmor(20 * lvl);
        heroAttribute.setDexterity(50 * lvl);
    }

    @Override
    public int standardAttack() {

        return (2 * super.standardAttack());
    }

    @Override
    public int energyAttack() {

        return (4 * super.energyAttack());
    }


}
