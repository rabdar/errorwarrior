package createHero.heroClass;

import createHero.Hero;

public class Warrior extends Hero {


    public Warrior(int lvl) {
        super();
        setHeroAttribute(lvl);
    }

    @Override
    public void setHeroAttribute(int lvl) {
        heroAttribute.setLvl(lvl);
        heroAttribute.setStrength(100 * lvl);
        heroAttribute.setEnergy(20 * lvl);
        heroAttribute.setLive(100 * lvl);
        heroAttribute.setArmor(100 * lvl);
        heroAttribute.setDexterity(10 * lvl);
    }

    @Override
    public int standardAttack() {
        return (super.standardAttack());
    }

    @Override
    public int energyAttack() {
        return (2 * super.energyAttack());
    }

}
