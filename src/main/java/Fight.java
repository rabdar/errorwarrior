import createHero.*;

import java.util.InputMismatchException;
import java.util.Scanner;

class Fight {
    void startFight(User user) {
        attackError(choiceHero(user));
        user.getUserInterface().userInterface(user);
    }

    private static void attackError(Hero fightingHero) {
        int lvlRound = fightingHero.getHeroLvl();
        boolean error;
        do {
            if (!((lvlRound % 5) == 0)) {
                attackErrorMessage(fightingHero, lvlRound);
            } else {
                switch (lvlRound) {
                    case 5:
                        attackJuniorJavaDeveloper();
                        System.out.println("You beat the Junior Java Developer!." +
                                "\nPerhaps you will meet with his more experienced friend in another");
                        break;
                    case 10:
                        attackMidJavaDeveloper();
                        System.out.println("You beat the Mid Java Developer!." +
                                "\nHe needs the support of a Senior to overcome you!");
                        break;
                    case 15:
                        attackSeniorJavaDeveloper();
                        System.out.println("Congratulations! You have freed the world from the ERROR. " +
                                "\nYou can play again.");
                        new GameOptions().gameOptions();
                        break;
                }
            }
            fightingHero.lvlUp();
            System.out.println("Congratulations! You completed this round " + lvlRound + "!");
            lvlRound += 1;
            error = decideToStillPlayOrBack();
        } while (error);
        {
        }
    }

    private static void attackErrorMessage(Hero fightingHero, int lvlRound) {

        boolean error = true;
        int errorMessageLive = 600 * lvlRound;
        int fightingHeroLive = fightingHero.getHeroLive();
        double fightingHeroEnergy = fightingHero.getHeroEnergy();
        System.out.println("You have attacked an ERROR MESSAGE.\n" +
                "\nPlease enter “S” if you want to do a normal attack or enter “E” if you want to do an energy attack.\n");
        do {
            System.out.println("HERO: " + fightingHeroLive + "HP " +
                    fightingHeroEnergy + "E ______________vs______________ ERROR Live: " + errorMessageLive + "HP" +
                    "\nAttack:");

            String choiceAttack = checkInputAttackItIsCorrect(fightingHeroEnergy, fightingHero);

            errorMessageLive = fightingHeroAttackErrorMessage(choiceAttack, errorMessageLive, fightingHero);

            if (!(errorMessageLive <= 0)) {
                fightingHeroLive = errorMessageAttackFightingHero(lvlRound, fightingHero, fightingHeroLive);
            } else {
                error = false;
            }

            fightingHeroEnergy = controlFightingHeroEnergy(fightingHeroEnergy, fightingHero, choiceAttack);

        } while (error);
        System.out.println("Congratulations! You bested the enemy easily!");
    }

    static double controlFightingHeroEnergy(double fightingHeroEnergy, Hero fightingHero, String choiceAttack) {
        if (fightingHeroEnergy < fightingHero.getHeroEnergy()) {
            fightingHeroEnergy += (fightingHero.getHeroEnergy() * 0.5);
        }
        if (choiceAttack.equals("E")) {
            fightingHeroEnergy = 0;
        }
        return fightingHeroEnergy;
    }

    static int errorMessageAttackFightingHero(int lvlRound, Hero fightingHero, int fightingHeroLive) {

        int ErrorMessageAttack = 1020 * lvlRound;
        int hitErrorMessageAttack = (ErrorMessageAttack - fightingHero.hitDefence());
        fightingHeroLive -= hitErrorMessageAttack;
        if (fightingHeroLive <= 0) {
            System.out.println("Unfortunately, the enemy was stronger, try again.");
            attackErrorMessage(fightingHero, lvlRound);
        }
        return fightingHeroLive;
    }

    static int fightingHeroAttackErrorMessage(String choiceAttack, int errorMessageLive, Hero fightingHero) {
        if (choiceAttack.equals("S")) {
            errorMessageLive = errorMessageLive - fightingHero.standardAttack();
        } else {
            errorMessageLive = errorMessageLive - fightingHero.energyAttack();
        }
        return errorMessageLive;
    }

    private static String checkInputAttackItIsCorrect(double fightingHeroEnergy, Hero fightingHero) {
        Scanner input = new Scanner(System.in);
        String choiceAttack;
        boolean error = true;
        do {
            switch (choiceAttack = input.nextLine().toUpperCase().trim()) {
                case "S":
                    error = false;
                    break;
                case "E":
                    if (fightingHeroEnergy == fightingHero.getHeroEnergy()) {
                        error = false;
                        break;
                    } else {
                        System.out.println("You don’t have enough energy. You need " + fightingHero.getHeroEnergy() +
                                "E of energy, that’s why your hero used only a normal attack.");
                        choiceAttack = "S";
                        error = false;
                    }
                    break;
                default:
                    System.out.println("The attack you’re trying to choose doest exist. Please enter [S] or [E].");
                    break;
            }
        }
        while (error);
        {
        }
        return choiceAttack;
    }

    // Method need to be completed yet
    private static void attackJuniorJavaDeveloper() {
        System.out.println("You attacked junior java developer");
    }

    // Method need to be completed yet
    private static void attackMidJavaDeveloper() {
        System.out.println("You attacked mid java developer");
    }

    // Method need to be completed yet
    private static void attackSeniorJavaDeveloper() {
        System.out.println("You attacked senior java developer");
    }

    private static boolean decideToStillPlayOrBack() {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter YES [Y] if you want to continue your game. " +
                "If you want to exit, just type a random keyboard key.");
        String decide = input.nextLine().toUpperCase().trim();
        return "YES".equals(decide) || "Y".equals(decide);
    }


    private static Hero choiceHero(User user) {
        System.out.println("Please choose the number of your hero from the list [].");
        for (Hero heroes : user.getHeroList()) {
            System.out.println("[" + (user.getHeroList().indexOf(heroes) + 1) + "]" + heroes.toString());
        }
        Scanner input = new Scanner(System.in);
        int choiceHero = 1;
        boolean error = true;
        do {
            try {
                choiceHero = input.nextInt() - 1;
                if (!(choiceHero >= 0 && choiceHero < user.getHeroList().size())) {
                    System.out.println("The number you’re trying to choose doesn’t exist. " +
                            "Please enter a number from the available hero list.");
                    choiceHero(user);
                }
                error = false;
            } catch (InputMismatchException e) {
                System.out.println("You entered the number in the wrong format.");
                input.nextLine();
            }
        } while (error);
        {
        }
        Hero fightingHero = user.getHeroList().get(choiceHero);
       System.out.println("Choose: " + fightingHero);
        return fightingHero;
    }
}
