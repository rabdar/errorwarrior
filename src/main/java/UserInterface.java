import java.util.Scanner;

public class UserInterface {

    void userInterface(User user) {

        System.out.println(
                "\n***SAVE*** [S]\n" +
                        "**FIGHT*** [F]\n" +
                        "**HEROES** [H]\n" +
                        "***EXIT*** [E]");

        while (true) {
            Scanner input = new Scanner(System.in);
            String option = input.nextLine().trim().toUpperCase();

            switch (option) {
                case "S":
                    new FileWriterGame().save(user);
                    System.out.println("game saved correctly");
                    userInterface(user);
                    break;
                case "F":
                    new Fight().startFight(user);
                    break;
                case "H":
                    System.out.println("Welcome to the user profile.\n" + user.toString());
                    userInterface(user);
                    break;
                case "E":
                    System.out.println("Game End");
                    break;
                default:
                    System.out.println("The option you’re trying to choose doest exist.\n" +
                            "Please enter a letter from [].");
                    continue;
            }
            break;
        }
    }
}
