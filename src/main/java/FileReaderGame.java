import createHero.*;
import createHero.heroClass.Archer;
import createHero.heroClass.Fairy;
import createHero.heroClass.Magician;
import createHero.heroClass.Warrior;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class FileReaderGame {
    public User load() {
        User user = new User();
        ArrayList<Hero> heroList = user.getHeroList();
        try (
                var reader = new BufferedReader(new FileReader("testFile.txt"))
        ) {
            String nextLine = reader.readLine();
            user.setName(nextLine);
            while ((nextLine = reader.readLine()) != null) {
                String[] hero = nextLine
                        .split(":\\s+");

                String[] heroAttribute = hero[1]
                        .replace("=","")
                        .replaceAll("[a-z]", "")
                        .split(", ");

                int[] heroAttribute1 = new int[6];

                for (int i = 0; i < heroAttribute.length; i++) {
                    heroAttribute1[i] =Integer.parseInt(heroAttribute[i]);
                }
                switch (hero[0]) {
                    case "Warrior":
                        Warrior warrior = new Warrior(heroAttribute1[0]);
                        heroList.add(warrior);
                        break;
                    case "Fairy":
                        Fairy fairy = new Fairy(heroAttribute1[0]);
                          heroList.add(fairy);
                        break;
                    case "Magician":
                        Magician magician = new Magician(heroAttribute1[0]);
                        heroList.add(magician);
                        break;
                    case "Archer":
                        Archer archer = new Archer(heroAttribute1[0]);
                        heroList.add(archer);
                        break;
                }
            }
            user.setHeroList(heroList);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return user;
    }
}
