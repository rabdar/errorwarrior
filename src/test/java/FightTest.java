import createHero.*;
import createHero.heroClass.Archer;
import createHero.heroClass.Fairy;
import createHero.heroClass.Magician;
import createHero.heroClass.Warrior;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


class FightTest {

    private Fight fight;
    private Hero warrior;
    private Hero fairy;
    private Hero magician;
    private Hero archer;

    @BeforeEach
    void setUp() {
        warrior = new Warrior(1);
        fairy = new Fairy(1);
        magician = new Magician(1);
        archer = new Archer(1);
        fight = new Fight();
    }

    /**
     * Check if hero get energy when round is over and his energy is below the normal value of energy.
     */
    @Test
    void checkIfHeroGetEnergyWhenRoundIsOver() {
        Double fightingHeroEnergy = fight.controlFightingHeroEnergy(0, warrior, "S");
        assertEquals(10, fightingHeroEnergy);

        fightingHeroEnergy = fight.controlFightingHeroEnergy(0, fairy, "S");
        assertEquals(80, fightingHeroEnergy);

        fightingHeroEnergy = fight.controlFightingHeroEnergy(20, archer, "S");
        assertEquals(40, fightingHeroEnergy);

        fightingHeroEnergy = fight.controlFightingHeroEnergy(40, magician, "S");
        assertEquals(80, fightingHeroEnergy);
    }

    /**
     * Check if fighting hero energy set to "0" when round is over and this round Hero use energy attack.
     */
    @Test
    void checkIfEnergySetTo0WhenUseEnergyAttack() {
        double fightingHeroEnergy = fight.controlFightingHeroEnergy(20, warrior, "E");
        assertEquals(0, fightingHeroEnergy);

        fightingHeroEnergy = fight.controlFightingHeroEnergy(160, fairy, "E");
        assertEquals(0, fightingHeroEnergy);

        fightingHeroEnergy = fight.controlFightingHeroEnergy(40, archer, "E");
        assertEquals(0, fightingHeroEnergy);

        fightingHeroEnergy = fight.controlFightingHeroEnergy(80, magician, "E");
        assertEquals(0, fightingHeroEnergy);
    }

    /**
     * Check how much live taken from enemy(ERROR MESSAGE) when hero use standard attack.
     */
    @Test
    void testHowMuchDamageTakeHeroWhenUseStandardAttack() {
        int errorMessageLive = fight.fightingHeroAttackErrorMessage("S", 600, warrior);
        assertEquals(500, errorMessageLive);
        errorMessageLive = fight.fightingHeroAttackErrorMessage("S", 600, fairy);
        assertEquals(500, errorMessageLive);
        errorMessageLive = fight.fightingHeroAttackErrorMessage("S", 600, archer);
        assertEquals(500, errorMessageLive);
        errorMessageLive = fight.fightingHeroAttackErrorMessage("S", 600, magician);
        assertEquals(500, errorMessageLive);
    }

    /**
     * Check how much live taken from enemy(ERROR MESSAGE) when hero use energy attack.
     */
    @Test
    void testHowMuchDamageTakeHeroWhenUseEnergyAttack() {
        int errorMessageLive = fight.fightingHeroAttackErrorMessage("E", 600, warrior);
        assertEquals(400, errorMessageLive);
        errorMessageLive = fight.fightingHeroAttackErrorMessage("E", 600, fairy);
        assertEquals(400, errorMessageLive);
        errorMessageLive = fight.fightingHeroAttackErrorMessage("E", 600, archer);
        assertEquals(400, errorMessageLive);
        errorMessageLive = fight.fightingHeroAttackErrorMessage("E", 600, magician);
        assertEquals(400, errorMessageLive);
    }

    /**
     * Check how much Live taken from hero when enemy(ERROR MESSAGE) attack hero.
     */
    @Test
    void testHowMuchDamageTakeEnemyWhenAttackFightingHero() {
        int fightingHeroLive = fight.errorMessageAttackFightingHero(1, warrior, warrior.getHeroLive());
        assertEquals(80, fightingHeroLive);
        fightingHeroLive = fight.errorMessageAttackFightingHero(1, fairy, fairy.getHeroLive());
        assertEquals(80, fightingHeroLive);
        fightingHeroLive = fight.errorMessageAttackFightingHero(1, archer, archer.getHeroLive());
        assertEquals(80, fightingHeroLive);
        fightingHeroLive = fight.errorMessageAttackFightingHero(1, magician, magician.getHeroLive());
        assertEquals(80, fightingHeroLive);
    }
}